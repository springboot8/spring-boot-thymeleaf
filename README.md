# Spring-boot-thymeleaf

Exemplo de API Springboot com thymeleaf. 

## Tecnologias utilizadas

* Java, versão: 8
* Maven
  * Spring Boot, versão: 2.1.13.RELEASE
  * Spring Data Jpa
  * Spring boot devtools
  * Thymeleaf

Start inicial

## Urls

Todas as urls responderão no formato **JSON**.

## Home
#### Página inicial

```
GET - http://localhost:8091/home
```

Request
```
Nenhum parâmetro passado na requisição.
```

Response 
```
O retorno será a página index.html
```

#### Página de boas vindas

```
GET - http://localhost:8091/home/welcome
```

Request
```
Nenhum parâmetro passado na requisição.
```

Response 
```
O retorno será a página welcome.html
```

