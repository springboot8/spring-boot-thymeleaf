package com.spring.boot.thymeleaf.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/home")
public class HomeController {

	@GetMapping("welcome")
	public ModelAndView home() {
		System.out.println(getClass() + " - Springboot está trabalhando! Página Index invocada.");
		ModelAndView mav = new ModelAndView();
		mav.addObject("msg", "Hello word!");
		mav.setViewName("index");
		return mav;
	}
	
	@GetMapping("/welcome")
	public ModelAndView bemvindo() {
		System.out.println(getClass() + " - Página Bem vido, invocada.");
		ModelAndView mav = new ModelAndView();
		mav.addObject("msg", "Hello word!");
		mav.setViewName("welcome");
		return mav;
	}
	
}
